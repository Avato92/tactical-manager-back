import express from "express";

const app = express();
const port = 3000;
app.get("/", (req, res) => {
  res.send("Express + TypeScript Server is awesome!");
});

app.listen(port, () => console.log(`server is listening on ${port}`));

app.on("error", (err) => console.log(err));
