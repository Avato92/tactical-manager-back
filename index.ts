import type {
  ErrorRequestHandler,
  Request,
  Response as ExpressResponse,
} from "express";

const express = require("express");

const app = express();
const PORT: number = 3000;

app.use(express.json());

app.get("/", (req: Request, res: ExpressResponse) => {
  res.send("Express + TypeScript Server is awesome!");
});

app.listen(PORT, () => console.log(`server is listening on ${PORT}`));

app.on("error", (err: ErrorRequestHandler) => console.log(err));
